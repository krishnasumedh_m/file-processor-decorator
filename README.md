--> Designed and developed decorators to process files based on the decorator design pattern in java. 

--> The Decorator Classes perform statistics computation such as word frequency count, line count etc. on the given files.

--> Each Decorator class adds additional functionality to the existing classes on top of them. 

-->    https://en.wikipedia.org/wiki/Decorator_pattern
