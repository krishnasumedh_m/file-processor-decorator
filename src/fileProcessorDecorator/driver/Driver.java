package fileProcessorDecorator.driver;

import fileProcessorDecorator.fileOperations.FileProcessor;
import fileProcessorDecorator.fileOperations.FileProcessorAbstractBase;
import fileProcessorDecorator.fileOperations.InputDetails;
import fileProcessorDecorator.fileOperations.ParagraphDecorator;
import fileProcessorDecorator.fileOperations.SentenceDecorator;
import fileProcessorDecorator.fileOperations.WordDecorator;
import fileProcessorDecorator.fileOperations.WordFrequencyDecorator;
import fileProcessorDecorator.util.FileDisplayInterface;
import fileProcessorDecorator.util.Results;

public class Driver 
{
	String line = null;

	public static void main(String[] args) 
	{	
		FileProcessor fp;
		FileProcessorAbstractBase fpab;
		FileDisplayInterface fdIn;
		fdIn = new Results();
		  
		if( (args.length != 2))
		   {
			 System.err.println("Invalid Arguments! Format : <input.txt> <output.txt>\n");
			 System.exit(0);
		   }
		
		fp = new FileProcessor(args[0]);
		InputDetails  id = new InputDetails ();
		id.readL(fp);
	
		fpab  = new SentenceDecorator(new ParagraphDecorator());
	    fpab  = new WordDecorator(fpab);
	    fpab  = new WordFrequencyDecorator(fpab);
	    fpab.retrieveInput(id, (Results)fdIn);
		
	    ((Results)fdIn).getResult();
	    ((Results)fdIn).writeToFile(args[1]);
        
		
	}
	
	
}
