package fileProcessorDecorator.fileOperations;

import fileProcessorDecorator.util.Results;

public abstract class FileProcessorAbstractBase 
{ 
	public abstract void retrieveInput(InputDetails id,Results r);
	
	
}
