package fileProcessorDecorator.fileOperations;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class InputDetails 
{
	int line;
	List<Character> Data = new ArrayList<>();
	List<String> Paragraphs = new ArrayList<>();
	List<String> Sentences = new ArrayList<>();
	List<String> Words = new ArrayList<>();
	Map<String,Integer> freq = new HashMap<>();

	public void readL(FileProcessor fp) 
	{	
		//populates the data structure with the content of the input file.
		while( -1 != ( line = fp.readLine() ) )
	    {
			char c = (char)line;
			Data.add(c);
	    }
		
	}
	
	public String printStart()
	{
		return "_START ---";
	}
	
	public String printEnd()
	{
		return "_END ---";
	}
	
	
	
	
	
}
