package fileProcessorDecorator.fileOperations;

import fileProcessorDecorator.util.Results;

public class ParagraphDecorator extends FileProcessorAbstractBase
{
	InputDetails id;
	FileProcessorAbstractBase fpab;
	//String ch = System.getProperty("line.separator");
	//char lineSeparator = ch.charAt(0);
	
	
	public ParagraphDecorator( )
	{	
	}
	
	public ParagraphDecorator(FileProcessorAbstractBase fp )
	{	
		this.fpab = fp;
	}
	
	
	@Override
	public void retrieveInput(InputDetails i,Results r) 
	{
		this.id = i;
		
	//	fpab.retrieveInput(i, r);
	//	performAction(id);
		
		printStart(id, r);
		performAction(id);
		printParagraphs(id,r);
		printEnd(id, r);	
	}
	
	
	public void performAction(InputDetails id)
	{
		
		boolean end = false;
		String str="";
		int loop=-1;
		
		for(char c:id.Data)
		{
			loop++;
			if(c=='\n' && end == true)
				continue;
			else if(c==' ' && end == true )
				continue;
			else
				end = false;
				
			
		    str = str + c;
			if(c=='.')
			{
				//System.out.println(loop+"  "+ (( id.Data.size() )-1 ));
			  if(loop != (id.Data.size())-1)
			  {	
				     //int index = id.Data.indexOf(c);
                  //System.out.println(id.Data.get(loop+1));
				
				  if(id.Data.get(loop+1)== '\n' )
				  {
					id.Paragraphs.add(str); 
					str = ""; 
					end = true;
					continue;
				  }
				
				  if(id.Data.get(loop+1)==' ' && id.Data.get(loop+2)==' ' )
				  {
					id.Paragraphs.add(str); 
					str = ""; 
					end = true;
					continue;
				  }
				
				
			  }//if
			  else
			  {
				    id.Paragraphs.add(str); 
					str = ""; 
					end = true;
					continue;
			  }
					
 
			}
		
		}
		
	}
	
	
	//Prints the Paragraphs stored in the Data Structure.
	public void printParagraphs(InputDetails id,Results r)
	{
		int i=1;
		//System.out.println("this : "+this);
	   for(String str : id.Paragraphs)
	   {
		   r.storeNewResult("Paragraph ->" +i+":"+str);
		   
		  //System.out.println("Paragraph ->" +i+":"+str);
			i++;
	   }
	}
	
	public void printStart(InputDetails i,Results r)
	{
		r.storeNewResult("\n---Paragraph_Decorator"+i.printStart());
	}
	
	public void printEnd(InputDetails i,Results r)
	{
		r.storeNewResult("---Paragraph_Decorator"+i.printEnd());
	}
	
	

}
