package fileProcessorDecorator.fileOperations;

import fileProcessorDecorator.util.Results;

public class SentenceDecorator extends FileProcessorAbstractBase
{
	InputDetails id;
	FileProcessorAbstractBase fpab;
	
	public SentenceDecorator ()
	{
		
	}
	
	public SentenceDecorator (FileProcessorAbstractBase fp )
	{
		this.fpab = fp;
	}
	
	
	@Override
	public void retrieveInput(InputDetails i, Results r) 
	{
		this.id = i;		
		fpab.retrieveInput(i, r);

		printStart(id, r);
		performAction(id);
		printSentences(id,r);
		printEnd(id, r);	
	}
	
	public void performAction(InputDetails id)
	{
		String str="";
		for(String s : id.Paragraphs)
		{
			char[] ch=s.toCharArray();  
			for(char c :ch)
			{
				str = str + c;
				if(c=='.')
				{
					id.Sentences.add(str);
					str = ""; 
				}
				
			}
			
		}
		
	}
	
	//Prints the Sentences stored in the Data Structure.
	public void printSentences(InputDetails id,Results r) 
	{
		int i=1;
		for(String s : id.Sentences)
		{  
			r.storeNewResult("Sentence: " +i+"  :"+s);
			//System.out.println("Sentence: " +i+"  :"+s);
			i++;
		}
		
	}
	
	public void printStart(InputDetails i,Results r)
	{
		r.storeNewResult("\n---Sentence_Decorator"+i.printStart());
	}
	
	public void printEnd(InputDetails i,Results r)
	{
		r.storeNewResult("---Sentence_Decorator"+i.printEnd());
	}
	
	

}
