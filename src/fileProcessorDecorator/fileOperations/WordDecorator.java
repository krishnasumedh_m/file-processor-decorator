package fileProcessorDecorator.fileOperations;

import fileProcessorDecorator.util.Results;

public class WordDecorator extends FileProcessorAbstractBase
{
	InputDetails id;
	FileProcessorAbstractBase fpab;
	

	public WordDecorator()
	{
		
	}
	
	public WordDecorator(FileProcessorAbstractBase f)
	{
		this.fpab = f;
	}

	@Override
	public void retrieveInput(InputDetails i, Results r) 
	{
		this.id = i;
		fpab.retrieveInput(i, r);
		
		printStart(id, r);
		performAction(id);
		printWords(id,r);
		printEnd(id, r);
		
	}
	
	public void performAction(InputDetails id)
	{
		
		for(String s:id.Sentences)
		{
			String[] sp = s.split(" ");
			for(int t=0;t<sp.length;t++)
			{
				sp[t] = sp[t].replace('.',' ');
				sp[t] = sp[t].trim();
				id.Words.add(sp[t]);
			}
		}
		
	}

	public void printWords(InputDetails id,Results r) 
	{
		//System.out.println("Words are :");
		for(String s:id.Words)
		{
			r.storeNewResult(s);
			//System.out.println(s);
		}
	}
	
	
	public void printStart(InputDetails i,Results r)
	{
		r.storeNewResult("\n---Word_Decorator"+i.printStart());
	}
	
	public void printEnd(InputDetails i,Results r)
	{
		r.storeNewResult("---Word_Decorator"+i.printEnd());
	}
	
	
	
}
