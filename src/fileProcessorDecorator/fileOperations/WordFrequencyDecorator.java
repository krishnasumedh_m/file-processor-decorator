package fileProcessorDecorator.fileOperations;

import java.util.Map;

import fileProcessorDecorator.util.Results;

public class WordFrequencyDecorator extends FileProcessorAbstractBase
{
	InputDetails id;
	FileProcessorAbstractBase fpab;
	
	public WordFrequencyDecorator()
	{
		
	}
	
	public WordFrequencyDecorator(FileProcessorAbstractBase f)
	{
		this.fpab = f;
	}

	@Override
	public void retrieveInput(InputDetails i, Results r) 
	{
		this.id = i;
		fpab.retrieveInput(i, r);
		
		printStart(id, r);
		performAction(id);
		printFreq(id,r);
		printEnd(id, r);
		
	}
	
	public void performAction(InputDetails id)
	{
		for(String str:id.Words)
		{
			int count = 0;
			for(int t=0;t<id.Words.size();t++)
			{
				String word = id.Words.get(t);
				if(str.equals(word))
				{
					count++;
				}	
			}
			id.freq.put(str, count);
			
		}
			
	}
	
	public void printFreq(InputDetails id,Results r)
	{
		for(Map.Entry<String, Integer> mp: id.freq.entrySet())
		{
			r.storeNewResult(mp.getKey()+" : "+mp.getValue());
			//System.out.println(mp.getKey()+" : "+mp.getValue());
			
		}
	}
	
	
	public void printStart(InputDetails i,Results r)
	{
		r.storeNewResult("\n---Word_Frequency_Decorator"+i.printStart());
	}
	
	public void printEnd(InputDetails i,Results r)
	{
		r.storeNewResult("---Word_Frequency_Decorator"+i.printEnd());
	}
	

}
